import React from 'react'
import Hero from '../components/aboutContent/hero';
import Article from '../components/aboutContent/article';


const About = () => {
  
  return (
    <main>
      <Hero />
      <Article />
    </main>
  )
}

export default About;
