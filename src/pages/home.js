import React from 'react';
import Converter from '../components/converter';


const Home = () => {
  return (
    <main>
      <Converter />
    </main>
  )
}

export default Home;
