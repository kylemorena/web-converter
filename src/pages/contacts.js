import React from 'react'
import ContactsForm from '../components/form/contactsForm';

const Contacts = () => {
  return (
    <main>
      <ContactsForm />
    </main>
  )
}

export default Contacts
