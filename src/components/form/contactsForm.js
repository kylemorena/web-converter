import React,{useState} from 'react';
import {Button,TextField,FormControlLabel,Checkbox,Grid,Typography,Container} from '@material-ui/core';
import {useStyles} from './useStylesForm';
import Axios from 'axios';

const ContactsForm = () => {
  const classes = useStyles();
  const [userData, setUserData] = useState({})

  const handleChange = (e) => {
    setUserData({...userData,[e.target.id]:e.target.value || e.target.checked || 'nessun dato'});
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if(userData && Object.keys(userData).length >= 3 && userData.constructor === Object)
    {
      console.log(userData);
      Axios.post('https://hookb.in/1gjbpJ63mPij002ykKg1',userData)
        .then(res=>{
          console.log(res.data);
        })
      setUserData({});
    }else{
      alert('Ciao, ti sei scordato di compilare il form');
    }
  }
  
  return (
    <Container maxWidth="sm" className={classes.container}>
      <Typography component="h1" variant="h5" align="center">
        Verrai contattato il prima possibile!
      </Typography>
      <form className={classes.form} noValidate onSubmit={handleSubmit}>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <TextField
              autoFocus
              required
              fullWidth
              variant="filled"
              color="primary"
              autoComplete="fname"
              id="firstName"
              label="Nome"
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              fullWidth
              variant="filled"
              autoComplete="lname"
              id="lastName"
              label="Cognome"
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12} color="secondary">
            <TextField
              required
              fullWidth
              variant="filled"
              autoComplete="email"
              id="email"
              label="Email"
              onChange={handleChange}
              className={classes.textField}
            />
          </Grid>
          <Grid item xs={12}>
            <FormControlLabel
              control={<Checkbox id="allowExtraEmail" color="primary" onChange={handleChange}/>}
              label="Sono interassato a ricevere promozioni di marketing e aggiornamenti via email."
            />
          </Grid>
        </Grid>
        <Button 
          type="submit" 
          fullWidth 
          variant="contained" 
          className={classes.submit}>
            Invia
        </Button>
      </form>
    </Container>
  );
}
export default ContactsForm;