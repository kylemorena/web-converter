import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  container: {
    color:theme.palette.common.black,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
    color:theme.palette.common.black,
  },
  textField:{
    color:theme.palette.common.black,
    borderRadius: '0.3em 0.3em 0',
  },
  submit: {
    color:theme.palette.common.black,
    background:theme.palette.background.paper,
    margin: theme.spacing(3, 0, 2),
    "&:hover":{
      background:theme.palette.primary.main,
    },
    "&:focus":{
      background:theme.palette.action.active,
      color: theme.palette.background.default,
    },
    "&:active":{
      background:theme.palette.action.active,
      color: theme.palette.background.paper,
    },
    "&:seclected":{
      background:theme.palette.action.active,
    }
  },
}));
