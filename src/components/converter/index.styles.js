import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  container: {
    color:theme.palette.common.black,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
  },
  title:{
    marginBottom: theme.spacing(4),
  },
  subTitle: {
    marginTop: theme.spacing(5),
  },
}));