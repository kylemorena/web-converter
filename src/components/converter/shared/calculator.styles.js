import styled from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';
import {Box} from '@material-ui/core';


export const useStyles = makeStyles((theme) => ({
  box: {
    color:theme.palette.common.black,
  },
  input:{
    margin: theme.spacing(1),
  },
  select: {
  },
}));

export const BoxStyled = styled(Box)`
  @media (max-width:768px){
    display:flex;
    flex-direction:column;
  }
`


export const Input = styled.input`
  background-color:transparent;
  border:2px solid #523d32;
  padding: 8px;
  border-radius:0.3rem;
  &:hover{
    border:2px solid #A68776;
  }
  &:focus{
    background-color:#A68776;
    border:2px solid #D9BBA9;
    outline:0px solid transparent;
  }
  &::placeholder{
    color:#D9BBA9;
    opacity: 0.8;
  }
`
export const Select = styled.select`
  background-color: transparent;
  border:2px solid #523d32;
  padding: 8px;
  border-radius:0.3rem;
  &:hover{
    border:2px solid #A68776;
  }
  &:focus{
    background-color:#A68776;
    border:2px solid #D9BBA9;
    outline:0px solid transparent;;
  }
  option{
    border:none;
  }
`