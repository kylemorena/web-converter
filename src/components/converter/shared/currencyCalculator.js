import React,{useRef} from 'react'
import {BoxStyled,Input,Select} from './calculator.styles';
import {useStyles} from './calculator.styles';

const CurrencyCalculator = (props) => {
  const {
    currencyData,
    setCurrency,
    rates,
    symbols,
    amount,
    setAmount,
    exchangeRates,
    setExchangeAmount 
  } = props;

  const classes = useStyles();

  const selectValue = useRef('');
  const inputValue = useRef(0);


  const exchangeCalculator = () => {
    if(selectValue.current.value===exchangeRates){
      setExchangeAmount(inputValue.current.value)
    }else{
      //Calcolo Tasso di Cambio
      if(rates[selectValue.current.value]>rates[exchangeRates]){
        const exchangeRate = rates[exchangeRates]/rates[selectValue.current.value];
        setExchangeAmount((inputValue.current.value*exchangeRate).toFixed(4));
      }else{
        setExchangeAmount((inputValue.current.value*rates[exchangeRates]).toFixed(4));
      }
    }
  }

  const changeAmount = (e) => {
    setAmount(e.target.value);
    exchangeCalculator();
  }
  const handleChangeOption = (e) => {
    setCurrency(e.target.value);
    exchangeCalculator();
  }

  return (
    <BoxStyled my={1} className={classes.box}>
      <Input 
        ref={inputValue} 
        type="number" 
        placeholder="Inserisci importo" 
        value={amount> 0 ? amount : ''} 
        onChange={changeAmount} 
        className={classes.input}  
        />
      <Select ref={selectValue} onChange={handleChangeOption}>
        {currencyData.map(res=>{
          return (
            <option key={res} value={res}>{symbols[res]}</option>
          )
        })}
      </Select> 
    </BoxStyled>
  )
}

export default CurrencyCalculator
