import React,{useState,useEffect,useCallback} from 'react';
import {Container,Typography} from '@material-ui/core';
import Axios from '../axios';
import CurrencyCalculator from './shared/currencyCalculator';
import Moment from 'moment';
import {useStyles} from './index.styles';

const API_KEY = process.env.REACT_APP_FIXER_API;

const Index = () => {
  const classes = useStyles();
  const lastWeek = Date.now() - 7 * 24 * 60 * 60 * 1000;
  const lastWeekHistorical = Moment(new Date(lastWeek)).format('YYYY-MM-DD');
  const [rates,setRates] = useState({})
  const [symbols,setSetSymbols] = useState({});
  const [historical,setHistorical] = useState([]);
  const [fromCurrencyData,setFromCurrencyData] = useState([])
  const [toCurrencyData,setToCurrencyData] = useState([])
  const [fromAmount,setFromAmount] = useState(0);
  const [toAmount,setToAmount] = useState(0);
  const [fromCurrency,setFromCurrency] = useState('');
  const [toCurrency,setToCurrency] = useState('');
  const [timeStamp,setTimeStamp] = useState(new Date());

  const callEndPoint = useCallback( async () => {
    try {
      const latest = await Axios.get(`latest?access_key=${API_KEY}`)
      const symbols = await Axios.get(`symbols?access_key=${API_KEY}`)
      const historical = await Axios.get(`${lastWeekHistorical}?access_key=${API_KEY}&symbols=EUR,USD`)
      const fromData = Object.keys(latest.data.rates).filter((res)=>
        res!==latest.data.base
      )
      const toData = Object.keys(latest.data.rates).filter((res)=>
        res!=='USD'
      )
      setRates(latest.data.rates);
      setSetSymbols(symbols.data.symbols);
      setTimeStamp(new Date(latest.data.timestamp* 1000));
      setFromCurrencyData([latest.data.base,...fromData]);
      setFromCurrency(latest.data.base);
      setToCurrencyData(['USD',...toData]);
      setToCurrency('USD');
      setHistorical(Object.entries(historical.data.rates))
    } catch (error) {
      console.log(error);
    }
  },[lastWeekHistorical])

  useEffect(() => {
    callEndPoint();
  }, [callEndPoint])



  return (
    <>
      <Container align="center" className={classes.container} >
        <Typography component="h1" variant="h1" className={classes.title}>
          Converti Valuta
        </Typography>
        {/* From */}
        <CurrencyCalculator 
          currencyData={fromCurrencyData} 
          setCurrency={setFromCurrency}
          rates={rates}
          symbols={symbols}
          amount={fromAmount}
          setAmount={setFromAmount}
          exchangeRates={toCurrency}
          setExchangeAmount={setToAmount}
        />
        {/* To */}
        <CurrencyCalculator 
          currencyData={toCurrencyData}
          setCurrency={setToCurrency}
          rates={rates}
          symbols={symbols}
          amount={toAmount} 
          setAmount={setToAmount}
          exchangeRates={fromCurrency}
          setExchangeAmount={setFromAmount}
        />
        <Typography variant="body1" >Ultimo aggiornamento: {timeStamp.toLocaleString()}</Typography>
        <Typography variant="h6" className={classes.subTitle}>
          Tasso di cambio nell'ultima settimana
        </Typography>
        {
          historical.map(res=>{
            return (
              <Typography variant="subtitle1" key={res[0]}>
                {symbols[res[0]]} = <span>{res[1]}</span>
              </Typography>
            )
          })
        }
      </Container>
    </>
  )
}

export default Index
