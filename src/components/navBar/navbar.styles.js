import styled from 'styled-components';

const NavBarStyle = styled.div`
  display:flex;
  justify-content: space-between;
  align-items: center;
  flex-wrap:wrap;
  padding:4px 24px;
  button{
    display:none;
    svg{
      padding:0;
      max-width:2.5em;
    }
    @media (max-width:768px){
    display:flex;
    }
  }
  svg{
    height:auto;
    width:6em;
  }
  nav{
    display:flex;
    justify-content: space-between;
    align-items: center;
    @media (max-width:768px){
      overflow:hidden;
      flex-direction:column;
      width:100%;
    }
  }
  .hide{
    max-height:0;
  }
  .show{
    max-height:300px;
  }
`

export {NavBarStyle};