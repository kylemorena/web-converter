import {useState} from 'react';
import {AppBar,IconButton,Link} from '@material-ui/core';
import { MdMenu,MdClose } from 'react-icons/md';
import {Link as LinkReact} from 'react-router-dom';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { FcCurrencyExchange } from "react-icons/fc";
import {NavBarStyle} from './navbar.styles';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      color: 'red'
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    link: {
      color:theme.palette.secondary.main,
      fontSize:18,
      padding:theme.spacing(1),
      "& a": {
        textDecoration: 'none'
      },
      "&:hover": {
        textDecoration: 'underline'
      }
    },
  }),
);

const Navbar = () => {
  const [isMenuOpen,setIsMenuOpen] = useState(false);
  const classes = useStyles();

  const handleClick = () => {
    setIsMenuOpen(!isMenuOpen);
  }

  return (
    <div className={classes.root}>
      <AppBar position="relative" className={classes.appBar}>
          <NavBarStyle isOpen={isMenuOpen}>
            <FcCurrencyExchange />
            <IconButton edge="start" color="inherit" aria-label="menu" onClick={handleClick}>
              {
                isMenuOpen ? <MdClose/> : <MdMenu/> 
              }
            </IconButton>
            <nav className={isMenuOpen ? 'show' : 'hide'}>
              <Link variant="button" className={classes.link} component={LinkReact} to="/" >
                Home
              </Link>
              <Link variant="button" className={classes.link} component={LinkReact} to="/about">
                Chi siamo
              </Link>
              <Link variant="button" className={classes.link} component={LinkReact} to="/contacts">
                Contatti
              </Link>
            </nav>
          </NavBarStyle>
          
      </AppBar>
    </div>
  )
}

export default Navbar
