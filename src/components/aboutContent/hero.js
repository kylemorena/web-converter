import React from 'react';
import {makeStyles,Typography,Container} from '@material-ui/core';
import LoremIpsum from '../content/loremIpsum';

const useStyles = makeStyles((theme) => ({
  heroContent: {
    padding: theme.spacing(3,0),
  },
}));

export default function Album() {
  const classes = useStyles();

  return (
    <>
      {/* Hero unit */}
      <div className={classes.heroContent} mb={3}>
        <Container>
          <Typography component="h1" variant="h1" align="center" color="textPrimary">
            {LoremIpsum.title}
          </Typography>
          <Typography component="h2" variant="h5" align="center" color="secondary" >
            {LoremIpsum.sentence}
          </Typography>
        </Container>
      </div>
    </>
  );
}