import React from 'react'
import {Grid,Container,Typography} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import LoremIpsum from '../content/loremIpsum';
import img1 from '../../assets/image1.jpg';
import img2 from '../../assets/image2.jpg';

const useStyles = makeStyles((theme) => ({
  container:{
    padding: theme.spacing(3,3),
    margin:theme.spacing(3,3),
    background: theme.palette.background.paper,
    borderRadius: '.1rem',
  },
  img: {
    borderRadius: '0.3rem',
    height: "100%",
    width: "100%",
    boxShadow: '1px 3px 5px',
  },
  item3: {
    order: 0,
    [theme.breakpoints.up('md')]: {
      order: 1,
    },
  },
  item4: {
    order: 1,
    [theme.breakpoints.up('md')]: {
      order: 0,
    },
  },
}));

const Article = () => {
  const classes = useStyles();
  return (
    <Container maxWidth="lg" className={classes.container}>
      <Grid container spacing={3} justify="center" alignItems="center">
        <Grid item xs={12} md={6}>
          <Typography variant="h6">
            {LoremIpsum.sentence}
          </Typography>
        </Grid>
        <Grid item xs={12} md={6}>
          <img src={img1} alt="prova" className={classes.img}/>
        </Grid>
        <Grid item xs={12} md={6}  className={classes.item3}>
          <Typography variant="h6">
            {LoremIpsum.sentence}
          </Typography>
        </Grid>
        <Grid item xs={12} md={6}className={classes.item4}>
          <img src={img2} alt="prova" className={classes.img}/>
        </Grid>
      </Grid>
    </Container>
  )
}

export default Article
