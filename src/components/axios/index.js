import Axios from 'axios'

const instance = Axios.create({
  baseURL: 'http://data.fixer.io/api/'
});

export default instance;
