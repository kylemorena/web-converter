import {Typography,Link} from '@material-ui/core';

const Copyright = () => {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      Copyright © {' '}
      <Link color="inherit" href="https://kyle-morena.web.app/" target="blank">
        Developed by <strong>Kyle Denver Morena</strong>
      </Link>{' '}
      {new Date().getFullYear()}.
    </Typography>
  );
}

export default Copyright;