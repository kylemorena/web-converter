import { createGlobalStyle } from "styled-components"

const GlobalStyleComponent = createGlobalStyle`
  main{
    display:flex;
    justify-content:center;
    align-items:center;
    flex-direction:column;
    min-height:100vh;
  }
`
export default GlobalStyleComponent;
