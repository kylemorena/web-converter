import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Home from './pages/home';
import About from './pages/about';
import Contacts from './pages/contacts';
import {CssBaseline} from '@material-ui/core'
import Navbar from './components/navBar/navbar';
import GlobalStyledComponent from './global/globalStyle';
import {Box} from '@material-ui/core';
import Copyright from './components/copyright';

const App = () => {
  return (
    <>
      <GlobalStyledComponent />
      {/* CssBaseline Reset default styling */}
      <CssBaseline/>
      <Router>
        <Navbar />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/contacts">
            <Contacts />
          </Route>
          <Route path="*">
          </Route>
        </Switch>
        {/* COPYRIGHT */}
        <Box my={3}>
          <Copyright />
        </Box>
      </Router>
    </>
  );
}

export default App;