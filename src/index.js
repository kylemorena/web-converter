import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import { ThemeProvider,createMuiTheme } from '@material-ui/core/styles';
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#D9BBA9',
    },
    secondary: {
      main: '#0D0D0D',
    },
    background: {
      paper: '#A68776',
      default: '#735443',
    },
    action: {
      active: '#F2E9D8',
    }
  },
});

ReactDOM.render(
  <React.StrictMode>
    {/* <Provider store={store}> */}
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
    {/* </Provider> */}
  </React.StrictMode>,
  document.getElementById('root')
);

