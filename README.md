This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.

### File.env.example
- Ottieni la tua chiave su [https://fixer.io/](https://fixer.io/)
- Ora il file .env.example dovrà essere rinominato in .env senza example
- Adesso dovrai cambiare la variabile REACT_APP_FIXER_API inserendo la tua
## Come avviare il progetto

- basta digitare il comando yarn start oppure npm start
- per buildare invece yarn build oppure npm run build




